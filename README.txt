Given two folders:

sqlsrv-8.x-1.x-dev/drivers/lib/Drupal/Driver/Database/sqlsrv

drupal-8.3.5/core/lib/Drupal/Core/Database/Driver

Drop the 

sqlsrv/drivers/lib/Drupal/Driver/Database/sqlsrv 

folder to the folder

drupal-8.3.5/core/lib/Drupal/Core/Database/Driver/

of your Drupal installation.

Note 

sqlsrv-8.x-1.x-dev is the folder provided by this git repo (branch 8.x-3.x)
drupal-8.3.5  is the folder of your Drupal 8.3.5 installation


