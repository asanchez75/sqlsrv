<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\sqlsrv\Delete
 */

namespace Drupal\Core\Database\Driver\sqlsrv;

use Drupal\Core\Database\Query\Delete as QueryDelete;

class Delete extends QueryDelete { }