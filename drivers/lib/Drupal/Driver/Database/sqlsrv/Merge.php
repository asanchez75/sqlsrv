<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\sqlsrv\Merge
 */

namespace Drupal\Core\Database\Driver\sqlsrv;

use Drupal\Core\Database\Query\Merge as QueryMerge;

use Drupal\Core\Database\Driver\sqlsrv\Utils as DatabaseUtils;

use Drupal\Core\Database\Driver\sqlsrv\TransactionIsolationLevel as DatabaseTransactionIsolationLevel;
use Drupal\Core\Database\Driver\sqlsrv\TransactionScopeOption as DatabaseTransactionScopeOption;
use Drupal\Core\Database\Driver\sqlsrv\TransactionSettings as DatabaseTransactionSettings;

use Drupal\Core\Database\Query\InvalidMergeQueryException;

use PDO as PDO;
use Exception as Exception;
use PDOStatement as PDOStatement;

class Merge extends QueryMerge {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // We don't need INSERT or UPDATE queries to trigger additional transactions.
    $this->queryOptions['sqlsrv_skip_transactions'] = TRUE;
    return parent::execute();
  }

}