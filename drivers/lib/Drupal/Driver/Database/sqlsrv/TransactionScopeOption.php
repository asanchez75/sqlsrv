<?php

namespace Drupal\Core\Database\Driver\sqlsrv;

class TransactionScopeOption extends Enum {
  const RequiresNew = 'RequiresNew';
  const Supress = 'Supress';
  const Required = 'Required';
}